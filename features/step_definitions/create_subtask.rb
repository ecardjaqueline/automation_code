Given("I am at the Subtask dialog from my task {string}") do |task1|
    @tasklist_page.abrir_popup(task1)  
    @subtask_page.wait_for_modal
  end
  
  When("I inform subtask description {string} and the due date {string}") do |description, due_date|
    @subtask_page.new_sub_task(description,due_date)
  end
  
  Then("I should see {string} on the subtask list") do |my_subtask|
    expect(find('.modal-content table tr.ng-scope')).to have_content my_subtask
    sleep 5
  end