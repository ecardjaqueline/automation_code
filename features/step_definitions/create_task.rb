
Given("I am at the login page") do
    @login_page.load
    sleep 5
    
  end
  
  When("I log at the system with {string} and {string}") do |login_email, login_senha|
    @login_page.login(login_email,login_senha)
    sleep 5
    
  end
  
  Then("I see the message {string}") do |msg_success|
    expect(@home_page.welcome_msg.text).to eql msg_success
    
  end

  Then("when I click on My tasks button") do
    @home_page.mytask_btn.click
  end  
#   
  Then("I should see the message {string}") do |hello_user_msg|
    expect(@tasklist_page.text).to eql hello_user_msg
    sleep 5
  end

  Given("I create a new task called {string}") do |task|
    @tasklist_page.add_newtask(task)
    sleep 10
  end
  
  Then("I should see {string} on the list of created tasks") do |mytask|
      expect(find('.table tbody')).to have_content mytask 
  end

  Then("I should not see {string} on task list") do |mytask|
    
    if expect(find('.table tbody')).to have_content mytask 
        puts 'O sistema não deve aceitar inserir registros com menos de 3 caracteres ou registros com mais de 250 caracteres' 
    end

  end


