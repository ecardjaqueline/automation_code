
class SubtaskPage < SitePrism::Page
    element :modal, '.modal-content'
    element :title, '.modal-title'
    element :name_task, '#edit_task'
    element :sub_task, '#new_sub_task'
    element :due_date, '#dueDate'
    element :add_btn, '#add-subtask'
    elements :subtask_list, '.table tbody'
    elements :close_btn, '.btn btn-primarys'
 
    def new_sub_task(description,date)
         sub_task.set description
         due_date.set date
         add_btn.click
         sleep 10
    end
 
 
 
 end