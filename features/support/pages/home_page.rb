
class HomePage < SitePrism::Page

    element :welcome_msg, '.alert'
    element :nav_bar, '.nav.navbar-nav'
    element :mytask_btn, '.nav.navbar-nav a[href="/tasks"]'
  
end