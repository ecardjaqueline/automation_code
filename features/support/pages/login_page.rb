

class LoginPage < SitePrism::Page

    set_url '/users/sign_in'

    element :campo_email, '#user_email'
    element :campo_senha, '#user_password'
   

    def login(email,senha)
        campo_email.set email
        campo_senha.set senha
        
        click_button 'Sign in'
        sleep 5
    end



end