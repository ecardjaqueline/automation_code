
class TaskListPage < SitePrism::Page
    set_url '/tasks'
    element :top_msg, 'h1'
    element :new_task, '#new_task'
    element :add_btn, '.input-group-addon'
    elements :todo_list, '.table tbody'

    def add_newtask(name_task)
        new_task.set name_task
        add_btn.click
    end

    
    def remove_task(name_task)
         registro_remove = find('.table tbody tr', text: name_task)
         registro_remove.find('button.btn.btn-xs.btn-danger').click
         sleep 5
    end

    def abrir_popup(my_task)
        task = find('.table tbody tr', text: my_task)
        task.find('button.btn.btn-xs.btn-primary.ng-binding').click
        sleep 5
    end

end