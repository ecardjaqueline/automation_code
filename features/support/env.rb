require 'capybara'
require 'capybara/cucumber'
require 'site_prism'

Capybara.configure do |config|
    config.default_driver = :selenium
    config.app_host = 'https://qa-test.avenuecode.com/'

end

Capybara.default_max_wait_time = 5