Before do
    @login_page = LoginPage.new
    @home_page = HomePage.new
    @tasklist_page = TaskListPage.new
    @subtask_page = SubtaskPage.new
end

Before ('@autenticaded') do
    @login_page.load
    @login_page.wait_for_campo_email
    @login_page.login('jaqsantos123@teste.com.br','12345678')
    @home_page.wait_for_mytask_btn
    @home_page.mytask_btn.click
end

Before ('@taskcreated') do
    @tasklist_page.add_newtask('taskTeste')
    sleep 5
end
