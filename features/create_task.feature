Feature: Create Task

    As a ToDo App user
    I should be able to create a task
    So I can manage my tasks

Background: Login page
    Given I am at the login page
    When I log at the system with "jaqsantos123@teste.com.br" and "12345678"
    Then I see the message "Signed in successfully."
    And when I click on My tasks button
    
Scenario: Message at the top
    Then I should see the message "Hey Jaqueline, this is your todo list for today:"

Scenario: Create new task
    Given I create a new task called "Jaque's task"
    Then I should see "Jaque's task" on the list of created tasks

# @doing
# Scenario: Number of Characters validations
#     Given I have the tasks
#         |task|on|
#         |task|Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra nisl sed nulla maximus, ac consequat lacus commodo. Suspendisse vitae mauris libero. Nullam enim elit, dignissim vel metus quis, aliquet congue purus. Nunc dui elit, porta ut ma|
#     When I click on add button
#     Then I should not see those tasks on list

# @doing
# Scenario: Task with less than 3 characters
#     Given I create a new task called "on"
#     Then I should see the message "You need to create a task with more than 3 characters"


Scenario: Task with less than 3 characters
    Given I create a new task called "on"
    Then I should not see "on" on task list

Scenario: Task with more than 250 characters
    Given I create a new task called "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra nisl sed nulla maximus, ac consequat lacus commodo. Suspendisse vitae mauris libero. Nullam enim elit, dignissim vel metus quis, aliquet congue purus. Nunc dui elit, porta ut ma"
    Then I should not see "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pharetra nisl sed nulla maximus, ac consequat lacus commodo. Suspendisse vitae mauris libero. Nullam enim elit, dignissim vel metus quis, aliquet congue purus. Nunc dui elit, porta ut ma" on task list
    