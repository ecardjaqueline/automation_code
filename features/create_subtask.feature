Feature: Create SubTask

As a ToDo App user
I should be able to create a subtask
So I can break down my tasks in smaller pieces

Background: 
    Given I am at the Subtask dialog from my task "taskTeste"

@autenticaded @taskcreated @doing
Scenario: Create New Subtask    
    When I inform subtask description "New Subtask" and the due date "05/05/2018"
    Then I should see "New Subtask" on the subtask list

@autenticaded
Scenario: Description required
    When I inform subtask description "" and the due date "05/05/2018"
    Then I see the message "You should inform a SubTask description"

@autenticaded
Scenario: Due date required
    When I inform subtask description "Subtask 2" and the due date ""
    Then I see the message "You should inform a due_date"

# Scenario Outline: Required fields validation
#     Given I have those following informations:
#     | subtask_description | <subtask_description> |
#     | due_date            | <due_date>            |
#     When I inform subtask description "<subtask_description>" and the due date "<due_date>"
#     Then I see the message "<message>"

#     Examples:
#     | subtask_description| due_date |message|
#     |  | 25/05/2018  |You should inform a SubTask description|
#     |subtask1||You should inform a due_date| 



  